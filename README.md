# Project EmeraldValley

Video igra u kojoj se na mrežu postavljaju izgenerisana polja koja sadrže različite objekte (prirodne i veštačke) koji se međusobno sklapaju

## Developers

- [Radovan Božić, 172/2018](https://gitlab.com/mi18172)
- [Petar Barić, 430/2019](https://gitlab.com/petar_baric)
- [Saška Keneški, 298/2016](https://gitlab.com/keneshki)
- [Zlatko Keneški, 75/2018](https://gitlab.com/Dyslexoid)
